use serenity::{
    model::{
        channel::{
            Message,
            Reaction,
            ReactionType,
            Channel
        },
        id::{
            ChannelId,
            GuildId
        },
        gateway::Ready,
        guild::{
            GuildInfo,
            PartialGuild
        }
    },
    utils::MessageBuilder,
    prelude::*,
    http::{
        AttachmentType,
        GuildPagination
    }
};
use linkify::{LinkFinder, LinkKind};
use std::{
    collections::HashMap,
    env
};

macro_rules! vec_str {
    ($($x:expr),*) => (vec![$($x.to_string()),*]);
}

struct SelfUserIdContainer;

impl TypeMapKey for SelfUserIdContainer {
    type Value = u64;
}

struct Handler {
    pub whitelisted_channel_names: Vec::<String>
}

enum GuildContainer {
    Full(GuildInfo),
    Partial(PartialGuild)
}

impl Handler {
    pub fn new(
        whitelisted_channel_names: Vec::<String>
    ) -> Handler {
        Handler {
            whitelisted_channel_names: whitelisted_channel_names
        }
    }

    fn process_reaction_change(&self, ctx: Context, message: Message) {
        // on reaction update, check the numbers, if dislike - like ratio over 3:1 AND dislike count at least 2 dislikes, remove message being reacted to
        let data_read_lock = ctx.data.read();
        let self_user_id = data_read_lock.get::<SelfUserIdContainer>();
        if self_user_id.is_none() {
            eprintln!("Received message while not yet connected");
            return;
        }

        // ignore if author is self
        let self_user_id = *self_user_id.unwrap();
        let author_id = u64::from(message.author.id);
        if self_user_id != author_id {
            println!("Ignoring reaction on message posted by someone else");
            return;
        }

        let mut thumbs_up_count = 0;
        let mut thumbs_down_count = 0;

        for reaction in &message.reactions {
            let reaction_type = &reaction.reaction_type;
            match reaction_type {
                ReactionType::Unicode(unicode_char) => {
                    match &unicode_char[..] {
                        "👍" => thumbs_up_count = reaction.count - 1,
                        "👎" => thumbs_down_count = reaction.count - 1,
                        _ => {}
                    }
                },
                _ => {}
            }
        }

        // calculate the ratio, percentage of disliked
        let dislike_ratio = if thumbs_down_count == 0 { 0.0 } else { thumbs_down_count as f64 / (thumbs_up_count + thumbs_down_count) as f64 };
        
        println!("Reaction update, {} likes, {} dislikes, {:.1}% dislikes", &thumbs_up_count, &thumbs_down_count, dislike_ratio * 100.0);

        if thumbs_down_count >= 2 && dislike_ratio >= 0.75 {
            // too many dislikes, delete message
            println!("Too big dislike ratio, deleting reposted message");
            let res = message.delete(&ctx.http);
            if res.is_err() {
                eprintln!("Unable to delete reposted message");
            }
        }
    }

    fn get_name_from_channel(&self, channel: &Channel) -> String {
        match channel {
            Channel::Group(group_lock) => {
                let name = &group_lock.read().name;
                if name.is_some() {
                    name.as_ref().unwrap().clone()
                } else {
                    String::from("Unknown group")
                }
            },
            Channel::Category(category_lock) => (&category_lock.read().name).clone(),
            Channel::Private(private_lock) => (&private_lock.read().name()).clone(),
            Channel::Guild(guild_lock) => (&guild_lock.read().name).clone(),
            Channel::__Nonexhaustive => String::from("Unable to resolve")
        }
    }
}

impl EventHandler for Handler {
    fn message(&self, ctx: Context, msg: Message) {
        let data_read_lock = ctx.data.read();
        let self_user_id = data_read_lock.get::<SelfUserIdContainer>();
        if self_user_id.is_none() {
            eprintln!("Received message while not yet connected");
            return;
        }

        // ignore if author is self
        let self_user_id = *self_user_id.unwrap();
        let author_id = u64::from(msg.author.id);
        if self_user_id == author_id {
            println!("Ignoring message posted by self");
            return;
        }

        // check if channel name in whitelist
        let channel = msg.channel_id.to_channel(&ctx.http);
        if channel.is_err() {
            eprintln!("Unable to retrieve channel for channel ID {}", &msg.channel_id);
            return;
        }
        let channel = channel.unwrap();
        let channel_name = self.get_name_from_channel(&channel);

        if !self.whitelisted_channel_names.contains(&channel_name) {
            println!("Channel {} not in whitelisted", &channel_name);
            return;
        }

        // pull all links
        let message_content = msg.content.clone();
        let mut finder = LinkFinder::new();
        finder.kinds(&[LinkKind::Url]);
        let message_links: Vec<_> = finder.links(&message_content).map(|link| { link.as_str() }).collect();

        // pull all attachments
        let attachment_links: Vec<_> = msg.attachments.iter().map(|attachment| { &attachment.url[..] }).collect();

        println!("Got new message in channel {} ({} links, {} attachments)" , &channel_name, &message_links.len(), &attachment_links.len());

        if message_links.len() == 0 && attachment_links.len() == 0 {
            println!("Not a meme post (no links, no attachments)");
            return;
        }

        // post to all monitored channels
        let mut target_channels: Vec<ChannelId> = Vec::with_capacity(self.whitelisted_channel_names.len() - 1);
        let original_guild_id = msg.guild_id;
        if original_guild_id.is_none() {
            eprintln!("Unable to get guild ID of the original message");
            return;
        }
        let original_guild_id = original_guild_id.unwrap();
        let mut guilds: Vec<GuildContainer> = Vec::new();
        let guilds_part = ctx.http.get_guilds(&GuildPagination::Before(original_guild_id), 20);
        if guilds_part.is_err() {
            eprintln!("Unable to get the first part of guild list");
        }
        let guilds_part = guilds_part.unwrap();
        for guild_info in guilds_part {
            guilds.push(GuildContainer::Full(guild_info));
        }
        let guilds_part = ctx.http.get_guilds(&GuildPagination::After(original_guild_id), 20);
        if guilds_part.is_err() {
            eprintln!("Unable to get the second part of guild list");
        }
        let guilds_part = guilds_part.unwrap();
        for guild_info in guilds_part {
            guilds.push(GuildContainer::Full(guild_info));
        }
        let original_guild = ctx.http.get_guild(u64::from(original_guild_id));
        if original_guild.is_err() {
            eprintln!("Unable to get original guild info");
        } else{
            let original_guild = original_guild.unwrap();
            guilds.push(GuildContainer::Partial(original_guild));
        }
        for considered_channel_name in &self.whitelisted_channel_names {
            if considered_channel_name[..] != channel_name[..] {
                for guild_info in &guilds {
                    let guild_name = match guild_info {
                        GuildContainer::Full(guild_info) => guild_info.name.clone(),
                        GuildContainer::Partial(guild_partial) => guild_partial.name.clone()
                    };
                    let guild_id = match guild_info {
                        GuildContainer::Full(guild_info) => guild_info.id.clone(),
                        GuildContainer::Partial(guild_partial) => guild_partial.id.clone()
                    };
                    let channels = guild_id.channels(&ctx.http);
                    if !channels.is_ok() {
                        eprintln!("Unable to retrieve channels for guild {}", &guild_name);
                        continue;
                    }

                    let channels = channels.unwrap();
                    for (channel_id, channel_itself) in channels {
                        if channel_itself.name[..] == considered_channel_name[..] {
                            target_channels.push(channel_id);
                        }
                    }
                }
            }
        }

        let original_message_guild_id = msg.guild_id;
        println!("Publishing message to {} other channels", &target_channels.len());
        for target_channel in &target_channels {
            let resolved_channel = ctx.http.get_channel(u64::from(*target_channel));
            let mut this_channel_name: String = String::from("UNKNOWN");
            if resolved_channel.is_ok() {
                let resolved_channel = resolved_channel.unwrap();
                this_channel_name = self.get_name_from_channel(&resolved_channel);
            }
            println!("Publishing message to channel {}", &this_channel_name);
            let res = target_channel.send_message(&ctx.http, |message| {
                let guild_id = original_message_guild_id;
                let mut guild_name = String::from("UNKNOWN");
                if guild_id.is_some() {
                    let guild_id = guild_id.unwrap();
                    let guild_id = u64::from(guild_id);
                    let guild_id_num = u64::from(guild_id);
                    let guild_info = ctx.http.get_guild(guild_id_num);
                    if guild_info.is_err() {
                        eprintln!("Unable to get guild info for ID {}", &guild_id_num);
                    } else {
                        let guild_info = guild_info.unwrap();
                        guild_name = (&guild_info.name).clone();
                    }
                }

                let mut message_text = MessageBuilder::new()
                    .push("Posted by ")
                    .push_mono(msg.author.tag())
                    .push(" in channel ")
                    .push_bold(format!("#{}", &channel_name))
                    .push(" on server ")
                    .push_bold(&guild_name)
                    .build();

                for link in &message_links {
                    message_text += &format!("\nLink: {}", &link)[..];
                }

                for link in &attachment_links {
                    message.add_file(AttachmentType::Image(link));
                }

                message.content(message_text);
                
                message
            });
            if res.is_err() {
                eprintln!("Unable to post message in channel {}", &this_channel_name);
            } else {
                // add a like + dislike reaction
                println!("Message posted");

                println!("Adding reactions");
                let message = res.unwrap();
                let reactions = vec![
                    "👍",
                    "👎"
                ];
                for reaction in &reactions {
                    let res = message.react(
                        &ctx.http,
                        *reaction
                    );
                    if res.is_err() {
                        eprintln!("Unable to add {} reaction", reaction);
                        if message.delete(&ctx.http).is_err() {
                            eprintln!("Unable to delete the reposted message");
                        } else {
                            break;
                        }
                    }
                }
            }
        }

        println!("Message handled");
    }

    fn reaction_add(&self, ctx: Context, added_reaction: Reaction) {
        let message = added_reaction.message(&ctx.http);
        if message.is_err() {
            eprintln!("Unable to retrieve message for added reaction");
            return;
        }
        self.process_reaction_change(ctx, message.unwrap());
    }

    fn reaction_remove(&self, ctx: Context, removed_reaction: Reaction) {
        let message = removed_reaction.message(&ctx.http);
        if message.is_err() {
            eprintln!("Unable to retrieve message for removed reaction");
            return;
        }
        self.process_reaction_change(ctx, message.unwrap());
    }

    fn ready(&self, ctx: Context, ready_info: Ready) {
        println!("{} is connected", ready_info.user.name);

        use serenity::model::{
            gateway::Activity, user::OnlineStatus
        };
        ctx.set_presence(Some(Activity::listening("rusty pipes")), OnlineStatus::Online);

        ctx.data.write().insert::<SelfUserIdContainer>(u64::from(ready_info.user.id));
    }
}

fn main() {
    let token = env::var("DISCORD_TOKEN");
    if token.is_err() {
        eprintln!("Missing DISCORD_TOKEN environment variable");
        return;
    }
    let token = token.unwrap();

    let handler = Handler::new(vec_str![
        "memes-nsfw-version",
        "memes",
        "meme",
        "memísky",
        "🦄sdílej-memes🦄"
    ]);

    let mut client = Client::new(&token, handler).expect("Unable to create client");
    if let Err(reason) = client.start() {
        eprintln!("Bot error: {:#?}", reason);
    }
}
